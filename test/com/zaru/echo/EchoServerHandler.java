/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.zaru.echo;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handler implementation for the echo server.
 */
@Sharable
public class EchoServerHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = Logger.getLogger(EchoServerHandler.class.getName());
    
    //private static ChannelGroup channelGroup = new DefaultChannelGroup("");

    
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        
    	System.out.println("Client: "+ ctx.channel().remoteAddress() + " connected....");
    	
    	ctx.writeAndFlush("hello word222");
    	
    	//ctx.fireChannelActive();
    }
    
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object message) throws Exception {
    	
    	//channelGroup.add(ctx.channel());
		
		
		String msg = (String) message;
		
		System.out.println(msg);
		System.out.println(ctx.channel());
		System.out.println("Server Msg: " + msg);
		
		ctx.writeAndFlush(msg);
		
		//System.out.println(channelGroup.size());
		
		ctx.channel().close();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        logger.log(Level.WARNING, "Unexpected exception from downstream.", cause);
        ctx.close();
    }
}
